##############################################################################
# Production Stage                                                           #
##############################################################################
ARG POSTGRES_MAJOR_VERSION=13
ARG POSTGIS_MAJOR_VERSION=3
ARG POSTGIS_MINOR_RELEASE=1

FROM kartoza/postgis:$POSTGRES_MAJOR_VERSION-$POSTGIS_MAJOR_VERSION.${POSTGIS_MINOR_RELEASE} AS postgis-backup-production

RUN apt-get -y update; apt-get -y --no-install-recommends install cron python3-pip vim gettext \
    && apt-get -y --purge autoremove && apt-get clean \
    && rm -rf /var/lib/apt/lists/*
RUN pip3 install s3cmd && \
    curl https://raw.githubusercontent.com/eficode/wait-for/v2.1.3/wait-for -o wait-for -s  && \
    chmod +x wait-for && \
    mv wait-for /bin/wait-for
RUN touch /var/log/cron.log

ENV \
    PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"

ADD build_data /build_data
ADD scripts /backup-scripts
RUN chmod 0755 /backup-scripts/*.sh
RUN sed -i 's/PostGIS/PgBackup/' ~/.bashrc

WORKDIR /backup-scripts

ENTRYPOINT ["/bin/bash", "/backup-scripts/start.sh"]
CMD ["/scripts/docker-entrypoint.sh"]
